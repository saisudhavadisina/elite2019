#include <stdio.h>
int main(){
    int size;
    printf("Enter the no. of procsses:");
    scanf("%d",&size);
    int a[size],min = 100;
    printf("Enter the arrival times :");
    for(int i = 0;i < size;i++){
        scanf("%d",&a[i]);
        if(a[i] < min){
            min = a[i];
            
        }
    }
    int b[size],total_time=0;
    printf("Enter burst times:");
    for(int i = 0;i < size;i++){
        scanf("%d",&b[i]);
        total_time += b[i];
    }
    int complete_time = min;
    float tat = 0,wt = 0;
    for(int i = min;i < total_time;i++){
        int least = 10;
        int index;
        for(int j = 0;j < size;j++){
            if(i >= a[j] && b[j] < least && b[j] > 0){
                least = b[j]; 
                index = j;
            }
        }
        b[index]--;
        complete_time++;
        int temp = complete_time - a[index];
        tat += temp;
        int var = temp - b[index];
        wt += var;
        printf("Process :%d\t complete-time :%d\n",index+1,complete_time);
    }
    printf("Average Turn-aroun-time :%f\n",tat/size);
    printf("Average Waiting-time :%f\n",wt/size);
    return 0;
}
