import random

def deletion(input_list, choice):
    updated_list= input_list.remove(choice)
    return input_list

string = "AKQJT98765432"
input_list = [str(i) for i in string]
choice = random.choice(input_list)
print(deletion(input_list, choice))
