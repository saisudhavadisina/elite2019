Error 1:

--> src/main.rs:1:1
  |
1 | extern crate clap;
  | ^^^^^^^^^^^^^^^^^^ can't find crate

error: aborting due to previous error

For more information about this error, try `rustc --explain E0463`.
error: could not compile `my_proj`.

To learn more, run the command again with --verbose.

Solution :

[dependencies]
clap = "~3.0.0-beta.1"

Error 2:

error: failed to select a version for the requirement `clap = "~3.0.0-beta.1"`
  candidate versions found which didn't match: 2.33.0, 2.32.0, 2.31.2, ...
  location searched: crates.io index
required by package `my_proj v0.1.0 (/home/suda/practice/my_proj)`

Solution :

[dependencies]
clap = "~2.33.0-beta.1" or "~2.32.0-beta.1" or "~2.31.2-beta.1"

Error 3:

Compiling CSVCut v0.1.0 (/home/suda/russell/explorations/Practice/Rust/CSVCut)
error[E0463]: can't find crate for `argparse`
 --> src/main.rs:2:1
  |
2 | extern crate argparse;
  | ^^^^^^^^^^^^^^^^^^^^^^ can't find crate

error: aborting due to previous error

For more information about this error, try `rustc --explain E0463`.
error: could not compile `CSVCut`.

To learn more, run the command again with --verbose.

Solution:

step1 : search for the crate using "cargo search <crate_name>", if it is already installed else install the crate using the command "cargo install <crate_name>"
step2 : add in to cargo.toml

Error 4 :

error: specified package `rand v0.7.3` has no binaries

Solution:



Error 5 :

error: expected item, found keyword `let`
  --> src/main.rs:11:1
   |
11 | let url = "mysql://root:r00t@localhost:3306/Customer";
   | ^^^ expected item

error: aborting due to previous error

error: could not compile `my_connection`.

Solution :

-> This is a parser error and those rarely have codes because the surface of things that could have gone wrong is much larger.
-> Unhelpful "expected item" error when putting let binding outside of function

Error6: (Occured while doing postgres connection)

error[E0432]: unresolved import `postgres`
 --> src/main.rs:1:5
  |
1 | use postgres::{Client, NoTls};
  |     ^^^^^^^^ use of undeclared type or module `postgres`

error[E0277]: the `?` operator can only be used in a function that returns `Result` or `Option` (or another type that implements `std::ops::Try`)
  --> src/main.rs:3:22
   |
2  | / fn main(){
3  | |     let mut client = Client::connect("host=localhost user=postgres", NoTls)?;
   | |                      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ cannot use the `?` operator in a function that returns `()`
4  | |     
5  | |     client.batch_execute("
...  |
26 | |     }
27 | | }
   | |_- this function should return `Result` or `Option` to accept `?`
   |
   = help: the trait `std::ops::Try` is not implemented for `()`
   = note: required by `std::ops::Try::from_error`

error[E0277]: the `?` operator can only be used in a function that returns `Result` or `Option` (or another type that implements `std::ops::Try`)
  --> src/main.rs:5:5
   |
2  |  / fn main(){
3  |  |     let mut client = Client::connect("host=localhost user=postgres", NoTls)?;
4  |  |     
5  |  |     client.batch_execute("
   |  |_____^
6  | ||     CREATE TABLE person (
7  | ||     id      SERIAL PRIMARY KEY,
8  | ||     name    TEXT NOT NULL,
9  | ||     data    BYTEA
10 | ||     )
11 | ||     ")?;
   | ||_______^ cannot use the `?` operator in a function that returns `()`
...   |
26 |  |     }
27 |  | }
   |  |_- this function should return `Result` or `Option` to accept `?`
   |
   = help: the trait `std::ops::Try` is not implemented for `()`
   = note: required by `std::ops::Try::from_error`

error[E0308]: mismatched types
  --> src/main.rs:17:18
   |
17 |         &[&name, &data],
   |                  ^^^^^ expected `&str`, found enum `std::option::Option`
   |
   = note:   expected type `&&str`
           found reference `&std::option::Option<&[u8]>`

error[E0277]: the `?` operator can only be used in a function that returns `Result` or `Option` (or another type that implements `std::ops::Try`)
  --> src/main.rs:15:5
   |
2  |  / fn main(){
3  |  |     let mut client = Client::connect("host=localhost user=postgres", NoTls)?;
4  |  |     
5  |  |     client.batch_execute("
...   |
15 | /|     client.execute(
16 | ||         "INSERT INTO person (name, data) VALUES ($1, $2)",
17 | ||         &[&name, &data],
18 | ||         )?;
   | ||__________^ cannot use the `?` operator in a function that returns `()`
...   |
26 |  |     }
27 |  | }
   |  |_- this function should return `Result` or `Option` to accept `?`
   |
   = help: the trait `std::ops::Try` is not implemented for `()`
   = note: required by `std::ops::Try::from_error`

error[E0277]: the `?` operator can only be used in a function that returns `Result` or `Option` (or another type that implements `std::ops::Try`)
  --> src/main.rs:20:16
   |
2  | / fn main(){
3  | |     let mut client = Client::connect("host=localhost user=postgres", NoTls)?;
4  | |     
5  | |     client.batch_execute("
...  |
20 | |     for row in client.query("SELECT id, name, data FROM person", &[])? {
   | |                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ cannot use the `?` operator in a function that returns `()`
...  |
26 | |     }
27 | | }
   | |_- this function should return `Result` or `Option` to accept `?`
   |
   = help: the trait `std::ops::Try` is not implemented for `()`
   = note: required by `std::ops::Try::from_error`

error: aborting due to 6 previous errors

Some errors have detailed explanations: E0277, E0308, E0432.
For more information about an error, try `rustc --explain E0277`.
error: could not compile `postgresdemo1`.

Solution:

Error :

bash: intellij-idea-community: command not found

Solution:



Error 

error[E0425]: cannot find value `upto20` in this scope
  --> src/main.rs:23:22
   |
23 |     let hu: String = upto20[n / 100].to_string();
   |                      ^^^^^^ not found in this scope

error[E0308]: mismatched types
  --> src/main.rs:21:9
   |
20 | /     if n > 100 {
21 | |         convert2digits(n)
   | |         ^^^^^^^^^^^^^^^^^ expected `()`, found struct `std::string::String`
22 | |     }
   | |_____- expected this to be `()`
   |
help: try adding a semicolon
   |
21 |         convert2digits(n);
   |                          ^
help: consider using a semicolon here
   |
22 |     };
   |      ^

error: aborting due to 2 previous errors

Some errors have detailed explanations: E0308, E0425.
For more information about an error, try `rustc --explain E0308`.
error: could not compile `rust`.

To learn more, run the command again with --verbose.

Solution :

for E0425: Directly called the function instead of declaring upto20s function again

for E0308 : added return before the function call

Error:

malformed entry 1 in list file /etc/apt/sources.list.d/atom.list (component)

Solution:

Remove atom.list file in /etc/apt/sources.list.d

