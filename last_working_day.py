import calendar

def find_last_working_day(month, year):
    working_days = calendar.monthcalendar(year, month)[-1:][0][:5]
    return max(working_days)

if __name__ == "__main__":
    month, year = map(int, input().split())
    print(find_last_working_day(month, year))
