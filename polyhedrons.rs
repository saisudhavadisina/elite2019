fn main() {
    use std::collections::HashMap;
    use std::iter::Sum;
    use std::io::{stdin,stdout,Write};

    let mut polyHedrons = HashMap::new();
    //string inputs = Console.ReadLine();
    //string[] intlist = inputs.Split(new char[] {',', ' '});
    
    //foreach(string item in intlist) {
        //Console.WriteLine(item);
    //}
    
    polyHedrons.insert(String::from("Tetrahedron"), 4);
    polyHedrons.insert(String::from("Cube"), 6);
    polyHedrons.insert(String::from("Octahedron"), 8);
    polyHedrons.insert(String::from("Dodecahedron"), 12);
    polyHedrons.insert(String::from("Icosahedron"), 20);
    
    //println!(inputs);
    println!("{:?}", polyHedrons);
}
