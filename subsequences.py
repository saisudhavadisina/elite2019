def subsequences(arr,n):
    ans = []
    for i in range(0, n - 2):
        for j in range(i + 1, n - 1):
            for k in range(j + 1, n):
                ans.append([arr[i], arr[j], arr[k])
    return ans
if __name__ == '__main__':
    t = int(input())
    for i in range(t):
        n = int(input())
        arr = list(map(int,input().split()))
        print(subsequences(arr, n))