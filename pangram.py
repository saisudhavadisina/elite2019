def pangram(n):
    lst = [i for i in n]
    lowerCase = 0
    upperCase = 0
    lst1 = []
    for i in lst:
        if 97 <= ord(i) <= 122:
            lowerCase += 1
        elif 65 <= ord(i) <= 90:
            upperCase += 1
    if upperCase != 0 and lowerCase != 0:            
        return "YES"
    return "NO"

if __name__ == "__main__":          
    n = int(input())
    s = str(input())
    if n == len(s):
        res = pangram(s)
        print(res)
