#include <stdio.h>

void read(int *a, int n){
    printf("P.no\tBurstTime\n");
    for(int i = 0;i < n;i++){
        printf("%d\t\t", (i+1));
        scanf("%d", &a[i]);
    }
}

int main(){
    int n, t;
    printf("Enter number of process: ");
    scanf("%d", &n);
    int b[30],p[30], sum, wt[30];
    printf("Enter burst times\n");
    read(b, n);
    printf("Enter the time : ");
    scanf("%d", &t);
    
    for(int  i = 0;i < n;i++){
         p[i] = i + 1;
    }

    for(int i = 0;i < n;i++){
        sum = 0;
        for(int j = 0;j < i;j++){
            sum = t + b[j];
        }
        wt[i] = sum;
        printf("The waiting time pf process %d is %d\n",p[i],wt[i]);
    }

    for(int i = 0;i < n;i++){
        int x = wt[i] + b[i];
        printf("Turn around time iof process %d is %d\n",p[i],x);
    }
  
    return 0;
}
