use std::io;
use std::collections::HashMap;
type Record = HashMap<String, String>;

pub fn get_data()  -> (Vec<String>, Vec<Vec<String>>, Vec<Vec<String>>)  {
   
    let mut data = Vec::new();
    let mut headers = Vec::new();
    let mut columns = Vec::new();
    let mut rows = Vec::new();

    let mut csv_read = csv::Reader::from_reader(io::stdin());

    for i in csv_read.deserialize() {
        let record: Record = i.unwrap();
        data.push(record);
    }
   
    let keys = csv_read.headers().unwrap();
    for key in keys {
        headers.push(String::from(key));
    }

    for key in keys.iter() {
        let mut each_col = Vec::new();
        for i in data.iter() {
            for (k, val) in i {
                if k == key {
                    each_col.push(val.to_owned());
                }
            }
        }
        columns.push(each_col);
    }

   
     for i in 0..columns[0].len() {
        let mut each_row = Vec::new();
        for col in &columns {
            each_row.push(col[i].to_owned());
        }
        rows.push(each_row);
    }  

    (headers, columns, rows)
}
