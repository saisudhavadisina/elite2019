extern crate quickersort;

fn typecasted_column(column: &Vec<String>) -> Vec<f64> {
    let mut typecasted_col = Vec::new();
    for i in column.iter() {
        typecasted_col.push(i.parse::<f64>().unwrap_or(0.00)); // typecasting each value as f64 and appending it
    }
    typecasted_col
}

pub fn minimum(column: &Vec<String>) -> f64 {
    let typecasted_col = typecasted_column(column);
    typecasted_col.iter().cloned().fold(0. / 0., f64::min)
}

pub fn maximum(column: &Vec<String>) -> f64 {
    let typecasted_col = typecasted_column(column);
    typecasted_col.iter().cloned().fold(0. / 0., f64::max)
}

pub fn sum(column: &Vec<String>) -> f64 {
    let typecasted_col = typecasted_column(column);
    typecasted_col.iter().sum()
}

pub fn mean(column: &Vec<String>) -> f64 {
    let typecasted_col = typecasted_column(column);
    let sum: f64 = typecasted_col.iter().sum(); // calculating sum
    let mean: f64 = sum as f64 / column.len() as f64; // calculating mean
    mean
}

pub fn median(column: &Vec<String>) {
    let mut typecasted_col = typecasted_column(column);
    quickersort::sort_floats(&mut typecasted_col[..]); // sorting the vector of floats using quickersort
    let mid = typecasted_col.len() / 2;
    if typecasted_col.len() % 2 == 0 {
        (typecasted_col[mid - 1] + typecasted_col[mid]) / 2.0
    } else {
        typecasted_col[mid]
    }
}

pub fn standard_deviation(column: &Vec<String>) -> f64 {
    let typecasted_col = typecasted_column(column);
    let column_mean = mean(column);
    let mut variance: f64 = 0.0;
    for value in typecasted_col {
        let diff = value - column_mean;
        variance += diff * diff;
    }
    let _std_dev = (variance / (column.len() - 1) as f64).sqrt();
    _std_dev
}
