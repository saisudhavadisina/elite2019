mod number_descriptives;
mod input;
extern crate argparse;
extern crate csv;
use argparse::{ArgumentParser, Store};
use input::get_data;
use number_descriptives::largest_value;
use number_descriptives::smallest_value;
use number_descriptives::add_value;
use number_descriptives::mean;
use number_descriptives::median;
use number_descriptives::standard_deviation;
use number_descriptives::highest_frequency;
use number_descriptives::unique_elements;
use number_descriptives::frequency_elements;
use number_descriptives::does_contains_nulls;
use std::error::Error;
use std::result::Result;

fn main() -> Result<(), Box<dyn Error>> {
    let cols = get_data().1;
    let mut column = 1;
    let mut name = String::new();
    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut column)
            .add_option(&["-c", "--column"], Store, "column");
        ap.refer(&mut name)
            .add_option(&["-n", "--name"], Store, "name");
        ap.parse_args_or_exit();
    }
    let required_col : Vec<String> = cols[column - 1].clone();
    let _value = &required_col[0];
    if _value.parse::<u32>().is_ok() || _value.parse::<f64>().is_ok() {
        println!("largest : {}",largest_value(&required_col));
        println!("smallest : {}",smallest_value(&required_col));
        add_value(&required_col);
        println!("mean : {}", mean(&required_col));
        median(&required_col);
        standard_deviation(&required_col);
        unique_elements(&required_col);
        highest_frequency(&required_col);
        frequency_elements(&required_col);
        does_contains_nulls(required_col);
    }
    Ok(())
}
