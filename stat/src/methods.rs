extern crate quickersort;
fn typecasted_column(column: &Vec<String>)-> Vec<f64>{
     let mut typecasted_col = Vec::new();
    for i in column.iter() {
        typecasted_col.push(i.parse::<f64>().unwrap_or(0.00)); // typecasting each value as f64 and appending it
    }
    typecasted_col
}
pub fn math_operations(column: &Vec<String>) {
    let  typecasted_col = typecasted_column(column);
    let greater = typecasted_col
        .iter()
        .max_by(|a, b| a.partial_cmp(b).unwrap());
    let smaller = typecasted_col
        .iter()
        .min_by(|a, b| a.partial_cmp(b).unwrap());
    match smaller {
        Some(x) => println!("Smallest value: {:?}", x),
        None => println!("0.00"),
    }
    match greater{
        Some(x) => println!("Largest value: {:?}", x),
        None => println!("0.00"),
    }
    let sum: f64 = typecasted_col.iter().sum();
    println!("Sum: {:?}", sum);
}

fn get_frequency(vec: &Vec<f64>, element: f64) -> usize {
    vec.iter().filter(|&n| *n == element).count() // returns the frequency of an element in the vector
}

pub fn mean(column: &Vec<String>) -> f64 {
    let typecasted_col = typecasted_column(column);
    let sum : f64 = typecasted_col.iter().sum(); // calculating sum
    let mean_col : f64 = sum as f64 / column.len() as f64; // calculating mean
    println!("Mean: {:?}", mean_col);
    mean_col
}
pub fn median(column: &Vec<String>) {
    let mut typecasted_col = typecasted_column(column);
    quickersort::sort_floats(&mut typecasted_col[..]); // sorting the vector of floats using quickersort
    let mid = typecasted_col.len() / 2;
    if typecasted_col.len() % 2 == 0 {
        let median = (typecasted_col[mid - 1] + typecasted_col[mid]) / 2.0;
        println!("Median: {:?}", median);
    } else {
        println!("Median: {:?}", typecasted_col[mid]);
    }
}
pub fn getting_uniques(column: &Vec<String>)-> Vec<f64>{
     let typecasted_col = typecasted_column(column);
    let mut uniques = typecasted_col.clone();
    uniques.sort_by(|a, b| a.partial_cmp(b).expect("NaN in vector"));
    uniques.dedup();
    uniques
}
pub fn highest_frequency(column: &Vec<String>){
     let  typecasted_col = typecasted_column(column);
    let  uniques = getting_uniques(column);
    let mut frequencies: Vec<(&f64, usize)> = vec![]; // creating a vector to store the element and its frequency as a tuple
    for i in &uniques {
        frequencies.push((&i, get_frequency(&typecasted_col, *i)))
    }
    frequencies.sort_by_key(|k| k.1); // sorting the vector of tuples based on the frequency
    frequencies.reverse();
    let max_frequency = frequencies[0].1; // the frequency of most-repeated values
    let modes = frequencies.iter().filter(|n| n.1 == max_frequency);  
    println!(" ");
    println!("Mode: "); // printing the mode values
    for mode in modes {
        print!("{}  ", mode.0);
    }
}
pub fn unique_elements(column: &Vec<String>){
     let uniques = getting_uniques(column);
    println!("Uniques: "); // printing the unique values
    for unique in &uniques {
        print!("{} ", unique);
    }
}
pub fn frequency_elements(column: &Vec<String>){
     let typecasted_col = typecasted_column(column);
    let uniques = getting_uniques(column);
    println!(" ");
    println!("Frequency of all elements in a column:");
    for unique in &uniques{
        println!("{} : {}" , unique, get_frequency(&typecasted_col,*unique));  
    }
}

pub fn standard_deviation(column: &Vec<String>) -> f64 {
    let typecasted_col = typecasted_column(column);
    let column_mean = mean(column);
    let mut variance : f64 = 0.0;
    for i in typecasted_col {
        let j = i - column_mean;
        variance += j * j;
    }
    let _std_dev = (variance / (column.len() - 1) as f64).sqrt();
    println!("Standard Deviation : {}", _std_dev);
    _std_dev
}

pub fn does_contains_nulls(column : Vec<String>){
    let nulls:Vec<String> = column
        .into_iter()
        .filter(|x| x.is_empty())
        .collect();
    
    if nulls.len() > 0 {
        println!("Contains null values : True");
    }
    else {
        println!("Contains null values : False");
    }
}
