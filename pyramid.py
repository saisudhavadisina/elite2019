import sys
size = int(input())
width = 40

star = "*"
space = " "
nextLine = "\n"

def stars(n: int) -> str:
    return (2 * n + 1) * star

def alignment(n: int) -> str:
    return (width - n) * space

def pyramid(size: int) -> str:
    return nextLine.join(((alignment(row_num) + stars(row_num)) for row_num in range(size)))

print(pyramid(size))
