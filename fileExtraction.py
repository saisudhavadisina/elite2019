fname = input("Enter file name: ")
fh = open(fname)
result = []
for line in fh:
    if not line.startswith("From ") : 
        continue
    fl = line.split()
    result.append(fl[1:2])

res = list(j for i in result for j in i)

print("\n".join(i for i in res))
print("There were", len(res), "lines in the file with From as the first word")
