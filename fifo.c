#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define FIFOFILE "MYFIFO"
int main() {
   int f;
   int end_process;
   int stringlen;
   char read[80];
   char end_str[5];
   printf("FIFOCLIENT: Send messages, infinitely, to end enter \"end\"\n");
   f = open(FIFOFILE, O_CREAT|O_WRONLY);
   strcpy(end_str, "end");
   while (1) {
      printf("Enter string: ");
      fgets(read, sizeof(read), stdin);
      stringlen = strlen(read);
      read[stringlen - 1] = '\0';
      end_process = strcmp(read, end_str);
       if (end_process != 0) {
         write(f, read, strlen(read));
         printf("Sent string: \"%s\" and string length is %d\n", read, (int)strlen(read));
      } else {
         write(f, read, strlen(read));
         printf("Sent string: \"%s\" and string length is %d\n", read, (int)strlen(read));
         close(f);
         break;
      }
   }
   return 0;
}
