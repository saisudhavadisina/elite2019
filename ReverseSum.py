def reversedNum(n):
    lst = [int(i) for i in str(n)]
    lst1 = reversed(lst)
    return ''.join([str(i) for i in lst1])

def reverseSum(a, b):
    res = int(reversedNum(a)) + int(reversedNum(b))
    return reversedNum(res)
    
if __name__ == '__main__':
    n = int(input())
    for i in range(n):
        x, y = input().split()
        x, y = [int(i) for i in [x, y]]
        res = reverseSum(x , y).lstrip("0")
        print(res)