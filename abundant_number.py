def divisible(number):
    for i in range(1, number):
        return [i for i in range(1, number + 1) if (number % i == 0)]
def is_abundant(number):
    if sum(divisible(number)) > number:
        return True
    return False
