import random

input_list = []

def rollDice(n):
    return [random.randint(1, 6) for i in range(n)]

def rounds():
    lst = []
    while input() == "Yes":
        input_list = rollDice(5)
        print(input_list)
        print(upper_sectionScore)
        print(lower_sectionScore)
    return max(max(upper_sectionScore), max(lower_sectionScore))


def countValue(k, n):
    return k.count(n)
    
def kindOf3(k):
    if 3 in upper_sectionScore:
        return sum([i for i in k])
    return -1
    
def kindOf4(k):
    if 4 in upper_sectionScore:
        return sum([i for i in k])
    return -1
    
def fullHouse(k):
    if 3 and 2 in upper_sectionScore:
            return 25
    return -1 

def yahtzee(k):
    if 5 in upper_sectionScore:
        return 50
    return -1

def chance(k):
    return -1

def sequence(k):
    seq1 = [[1, 2, 3, 4], [2, 3, 4, 5], [3, 4, 5, 6]]
    seq2 = [[1, 2, 3, 4, 5], [2, 3, 4, 5, 6]]
    if k in seq1:
        return 30
    elif k in seq2:
        return 40
    return -1

def smStraight(k):
    if sequence(k) == 30:
        return 30 
    return -1

def lgStraight(k):
    if sequence(k) == 40:
        return 40 
    return -1

upper_sectionScore = [countValue(input_list, i) for i in input_list]
lower_sectionScore = [kindOf3(input_list), kindOf4(input_list), smStraight(input_list), lgStraight(input_list), fullHouse(input_list), yahtzee(input_list), chance(input_list)]

print(rounds())
