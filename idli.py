o, e = 1, 0

def flip(state : int) -> int:
    return 1 - state

def transform(queue : [int]) -> [int]:
    return [q % 2 for q in queue]

def distributable(queue : [int]) -> bool:
    return sum(queue) % 2 == 0

def distribute(queue :  [int]) -> int:
    queue = transform(queue)
    if not distributable(queue):
        return -1
    return distrib(queue)

def distrib(queue :  [int]) -> int:
    if len(queue) == 0 or queue.count(o) == 0:
        return 0
    if queue.startsWith(e) == 0:
        return distrib(queue[1:])
    return 2 + distrib([flip(queue[1]) + queue[2:]])
