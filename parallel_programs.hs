import Control.Parallel
main = a `par` b `par` c `pseq` print ( a + b + c)
    where
        a = fib 10
        b = 10
        c = fac 6

fac 0 = 1
fac n = n * fib (n - 1)

fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)
