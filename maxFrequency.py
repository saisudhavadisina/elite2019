fname = input("Enter file name: ")
fh = open(fname)
result = []
for line in fh:
    if not line.startswith("From ") : 
        continue
    fl = line.split()
    result.append(fl[1:2])

res = list(j for i in result for j in i)
count, s = 0, res[0] 
for i in res: 
    freq = res.count(i)
    if(freq > count):
        count = freq
        s = i

maxStr = [s, count]
print(" ".join(str(i) for i in maxStr))
