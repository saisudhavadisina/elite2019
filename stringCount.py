s = "Parrot egg not green"

vowels = "aeiou"
consonants = "bcdfghjklmnpqrstvwxyz"

def stringCount(s):
    words = [s.split(" ")]
    vc = [(str(count_group(_, vowels)), str(count_group(_, vowels))) for _ in words]
    return " ".join("".join(_) for _ in zip(*vc))

print(stringCount(s))
