from math import gcd

def find_lcm(n1, n2):
    return (n1 * n2) // gcd(n1, n2)

def stopping_time(jump_time, k, n):
    n1 = jump_time[0]
    n2 = jump_time[1]
    lcm = find_lcm(n1, n2)
    for i in range(2, n):
        lcm = find_lcm(lcm, jump_time[i])
    result = lcm * k
    return result % 1000000007

if __name__ == "__main__":
    n = int(input())
    jump_time = list(map(int, input().split()))
    k =int(input())
    print(stopping_time(jump_time, k, n))