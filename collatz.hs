nextCollatz :: Int -> Int

nextCollatz n = if even n then (quot n 2) else (3 * n + 1)
collatzSeq :: Int -> Int
"""collatzSeq 4 = [4, 2, 1 ]
collatzSeq n = n : collatzSeq (nextCollatz n)
"""
collatzSeq n = if n == 4 then [4, 2, 1]
                else collatzSeq $ nextCollatz n1
main = do
    print (collatzSeq 12)
