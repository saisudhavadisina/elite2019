#include <stdio.h>

int factors(int n){
    int count = 0;
    for(int i = 1; i < n + 1; i++)
        if(n % i == 0)
            count++;
    return count;
}

int isPrime(int num) {
    int i, p = 0;
    for (i = 2; i < num; i++) {
        if (num % i == 0) {
            p++ ;
        }
    }
    if (p == 0) 
        return 1;
    else
        return 0;
}

int primeFactors(int n){
    int count = 0;
    for(int i = 2; i < n; i++)
        if(isPrime(n))
            count++;
    return count;
}

int strangeNumbers(int n, int k){
    int count = 0;
    for(int i = 1; i < 1000000000; i++)
        if(factors(i) == n && primeFactors(i) == k){
            count++;
            break;
        }
    return count;
}

int main(void) {
    int t, n, k;
    scanf("%d", &t);
    for(int i = 0; i < t; i++){
        scanf("%d %d", &n, &k);
        printf("%d", strangeNumbers(n, k));
    }
	return 0;
}


