fname = input("Enter file name: ")

fh, rs, add, count = open(fname), [], 0, 0

for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : 
        continue
    count = count + 1
    add += float(line[21:])
    
res = add / count
    
print("Average spam confidence:", res)
