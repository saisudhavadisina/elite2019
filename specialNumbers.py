def consecutive(n):
    return True if '123' in str(n) or '234' in str(n) or '345' in str(n) or '456' in str(n) or '567' in str(n) or '678' in str(n) or '789' in str(n) else False

def specialNumbers(L, R):
    nums = [i for i in range(L, R) if consecutive(i)]
    return len(nums)

if __name__ == '__main__':
    L, R = map(int, input().split())
    result = specialNumbers(L, R) 
    print(result)