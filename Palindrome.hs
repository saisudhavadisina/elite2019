isPalindrome xs = (head xs == last xs) && isPalindrome(tail(init(xs)))
main = do
     print(isPalindrome [1, 2, 1])
