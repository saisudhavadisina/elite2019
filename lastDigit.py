def power(a, b):
    return a ** b

def lastDigit(a, b):
    n = int(power(a, b))
    lst = [i for i in str(n)]
    length = len(lst)
    return int(lst[length - 1])

if __name__ == "__main__":
    x, y = input().split()
    a, b = [int(i) for i in [x, y]]
    res = lastDigit(a, b)
    print(res)
