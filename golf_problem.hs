import Data.Maybe (fromJust)

convert score = lookup score [("triple bogey", 3), ("double bogey", 2), ("bogey", 1), ("par", 0), ("birdie", -1), ("eagle", -2), ("albatross", -3)]

convertCard card = (sum [fromJust (convert x) | x <- card])

score n 
    | n == 0 = "YouScored par for the course"
    | n < 0 = "You scored " ++ (show $ abs n) ++ " under par for the course"
    | n > 0 = "You scored " ++ (show n) ++ " over par for the course"

scoreCard card = score $ convertCard card

allScores cards = [scoreCard card | card <- cards]

pars = [4, 4, 5, 3, 4, 4, 3, 5, 5, 3, 5, 4, 4, 4, 4, 3, 4, 4]
cards = [["eagle", "bogey", "par", "bogey", "double-bogey", "birdie","bogey", "par", "birdie", "par", "par", "par", "par", "par","bogey", "eagle", "bogey", "par"],
         ["par", "birdie", "par", "par", "par","bogey", "eagle", "eagle", "double-bogey", "birdie", "eagle", "birdie", "birdie", "bogey", "par", "bogey", "double-bogey", "birdie"],
         ["birdie", "eagle", "bogey", "par", "bogey", "double-bogey", "birdie","birdie", "albatross", "triple-bogey", "birdie", "bogey", "par", "birdie", "par", "birdie", "birdie", "birdie"]
        ]

main = do
    print $ allScores cards
