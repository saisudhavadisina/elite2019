lst = ["1234", "123"]

def lesser_than(lst):
    list1 = [int(i) for i in lst[0]]
    list2 = [int(i) for i in lst[1]]
    list2.pop(list2.index(min(list2)))
    list1.pop(list1.index(max(list1)))
    return [list1, list2]

print(lesser_than(lst))
