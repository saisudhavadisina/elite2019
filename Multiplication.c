#include <stdio.h>

int main(){
    int n, k, count = 0, arr[200][200];
    scanf("%d %d", &n, &k);
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            arr[i][j] = (i + 1) * (j + 1);
            if(arr[i][j] == k)
                count++;
        }
    }
    printf("%d\n", count);
    return 0;
}
